import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:podcasts/models/audio-state.dart';
import 'package:podcasts/models/constants.dart';
import 'package:podcasts/models/preferences.dart';

class MediaControlsWidget extends StatefulWidget {
  const MediaControlsWidget({Key? key}) : super(key: key);

  @override
  MediaControlsState createState() => MediaControlsState();
}

class MediaControlsState extends State<MediaControlsWidget> {
  @override
  Widget build(BuildContext context) {
    return Consumer<AudioState>(
      builder: (context, audioState, child) => Container(
        child: Column(
          children: [
            (audioState.isLive || audioState.currentlyPlaying == null)
                ? const Slider(onChanged: null, value: 1.0, min: 0.0, max: 1.0)
                : Slider(
                    value: audioState.positionValue,
                    activeColor: Constants.theme.colorScheme.secondary,
                    inactiveColor: Constants.theme.primaryColor,
                    onChanged: (double value) =>
                        audioState.seek(Duration(milliseconds: value.toInt())),
                    min: 0.0,
                    max: audioState.durationValueNonNegative,
                  ),
            _buildDistancedProgressAndDuration(audioState),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _settings(),
                Stack(
                  children: [
                    Container(
                      width: 64,
                      height: 64,
                      alignment: Alignment(0.4, 0.4),
                      child: const Text(
                        '15s',
                        style: TextStyle(fontSize: 12),
                      ),
                    ),
                    IconButton(
                      icon: const Icon(Icons.rotate_left),
                      iconSize: 64,
                      onPressed: () {
                        audioState.windUp(-15);
                      },
                    ),
                  ],
                ),
                IconButton(
                    focusColor: Constants.theme.colorScheme.secondary,
                    icon: audioState.isPlaying
                        ? Icon(Icons.pause_circle_filled,
                            color: Constants.theme.colorScheme.secondary)
                        : Icon(Icons.play_circle_filled,
                            color: Constants.theme.primaryColor),
                    iconSize: 96,
                    onPressed: audioState.isPlaying
                        ? () => audioState.pause()
                        : () => audioState.play()),
                Stack(
                  children: [
                    Container(
                        width: 64,
                        height: 64,
                        alignment: const Alignment(0.4, 0.4),
                        child: const Text(
                          '15s',
                          style: TextStyle(fontSize: 12),
                        )),
                    IconButton(
                      focusColor: Constants.theme.colorScheme.secondary,
                      icon: const Icon(Icons.rotate_right),
                      iconSize: 64,
                      onPressed: () {
                        audioState.windUp(15);
                      },
                    )
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  _settings() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _settingsButton(
            icon: Icons.repeat, preference: 'auto-play', text: 'auto play')
      ],
    );
  }

  _settingsButton(
      {var icon,
      required var preference,
      required var text,
      bool enabled = true}) {
    bool p = _preferences.get(preference);
    var c = enabled
        ? p
            ? Constants.theme.colorScheme.secondary
            : Constants.theme.primaryColor
        : Colors.grey;
    return Tooltip(
      message: text,
      textStyle: TextStyle(
        color: Constants.theme.colorScheme.secondary,
      ),
      child: IconButton(
        icon: Icon(icon, color: c, size: 32),
        iconSize: 32,
        onPressed: enabled
            ? () async {
                _preferences.toggle(preference);
                setState(() {});
              }
            : null,
      ),
    );
  }

  Widget _buildDistancedProgressAndDuration(AudioState audioState) {
    return Container(
      padding: EdgeInsets.only(left: 24.0, right: 24.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(audioState.positionText,
              style: const TextStyle(fontSize: 14.0),
              overflow: TextOverflow.visible),
          Text(audioState.durationText,
              style: const TextStyle(fontSize: 14.0),
              overflow: TextOverflow.visible)
        ],
      ),
    );
  }

  final Preferences _preferences = Preferences();
}
