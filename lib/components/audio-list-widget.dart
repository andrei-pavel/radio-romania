import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:share_plus/share_plus.dart';

import 'package:podcasts/components/linear-progress-indicator.dart';
import 'package:podcasts/models/audio-list.dart';
import 'package:podcasts/models/audio-state.dart';
import 'package:podcasts/models/constants.dart';
import 'package:podcasts/models/entry.dart';
import 'package:podcasts/models/preferences.dart';

class AudioListWidget extends StatefulWidget {
  const AudioListWidget({Key? key}) : super(key: key);

  @override
  AudioListState createState() => AudioListState();
}

class AudioListState extends State<AudioListWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<AudioList>(
      builder: (context, audioList, child) => () {
        final list = audioList.toList();
        return Expanded(
          child: Column(
            children: [
              () {
                double progress = audioList.networkProgress();
                if (progress < 1.0) {
                  return SmallHeightLinearProgressIndicator(progress);
                } else {
                  // Decide if last-retrieved needs to be updated.
                  var now = DateTime.now();
                  var lastRetrieved = _preferences.get('last-retrieved');
                  if (lastRetrieved == null ||
                      now.difference(DateTime.fromMillisecondsSinceEpoch(
                              lastRetrieved)) >=
                          const Duration(hours: 1)) {
                    _preferences.set(
                        'last-retrieved', now.millisecondsSinceEpoch);
                  }
                  return Container();
                }
              }(),
              Expanded(
                child: SmartRefresher(
                  controller: _refreshController,
                  header: WaterDropMaterialHeader(
                    backgroundColor: Constants.theme.colorScheme.secondary,
                    color: Constants.theme.primaryColor,
                    distance: 32,
                  ),
                  enablePullDown: true,
                  enablePullUp: false,
                  onRefresh: _onRefresh,
                  onLoading: _onLoading,
                  child: ListView.builder(
                    itemBuilder: (BuildContext context, int index) =>
                        _buildTiles(list[index]),
                    itemCount: list.length,
                  ),
                ),
              ),
            ],
          ),
        );
      }(),
    );
  }

  Widget _buildTiles(Entry root) {
    if (root.children.isEmpty) {
      return Consumer<AudioList>(
        builder: (context, audioList, child) => Consumer<AudioState>(
          builder: (context, audioState, child) => Column(
            children: [
              Container(
                  decoration: audioState.urlExists &&
                          audioState.currentlyPlaying.url == root.url
                      ? BoxDecoration(
                          color: Constants.theme.colorScheme.secondary)
                      : const BoxDecoration(),
                  child: ListTile(
                      title: Text(
                        root.title + (root.isNew ? " (nou)" : ""),
                        style: audioState.urlExists &&
                                audioState.currentlyPlaying.url == root.url
                            ? TextStyle(color: Constants.theme.primaryColor)
                            : root.isOld
                                ? TextStyle(color: Constants.fadedOutColor)
                                : root.isNew
                                    ? TextStyle(color: Constants.newColor)
                                    : const TextStyle(),
                      ),
                      trailing: root.playable
                          ? Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                IconButton(
                                  icon: Icon(Icons.favorite,
                                      color: root.favorite &&
                                              (!audioState.urlExists ||
                                                  audioState.currentlyPlaying
                                                          .url !=
                                                      root.url)
                                          ? Constants
                                              .theme.colorScheme.secondary
                                          : Constants.theme.primaryColor),
                                  onPressed: () {
                                    root.toggleFavorite();
                                    audioList.setFavorite(root);
                                    setState(() {});
                                  },
                                ),
                                IconButton(
                                  icon: const Icon(Icons.share),
                                  onPressed: () {
                                    Share.share(root.url);
                                  },
                                )
                              ],
                            )
                          : null,
                      onTap: root.playable
                          ? () {
                              audioState.setEntry(root);
                              audioState.play();
                            }
                          : null))
            ],
          ),
        ),
      );
    } else {
      return Card(
        margin: const EdgeInsets.all(8.0),
        color: Constants.cardColor,
        child: ExpansionTile(
          key: PageStorageKey<String?>(root.title),
          title: Text(root.title + (root.isNew ? " (nou)" : ""),
              style: root.isOld
                  ? TextStyle(color: Constants.fadedOutColor)
                  : root.isNew
                      ? TextStyle(color: Constants.newColor)
                      : const TextStyle(),
              overflow: TextOverflow.visible),
          children: root.children.map(_buildTiles).toList(),
        ),
      );
    }
  }

  _refresh() async {
    var audioList = Provider.of<AudioList>(context, listen: false);
    audioList.resetNetworkHits();
    audioList.getPodcasts(force: true);
    await audioList.networkComplete();
  }

  _onRefresh() async {
    await _refresh();
    _refreshController.refreshCompleted();
  }

  _onLoading() async {
    await _refresh();
    _refreshController.loadComplete();
  }

  final Preferences _preferences = Preferences();
  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);
}
