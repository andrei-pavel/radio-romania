import 'package:flutter/material.dart';

import 'package:podcasts/models/constants.dart';
import 'package:podcasts/models/preferences.dart';

class DrawerWidget extends StatefulWidget {
  const DrawerWidget({Key? key}) : super(key: key);

  @override
  DrawerWidgetState createState() => DrawerWidgetState();
}

class DrawerWidgetState extends State<DrawerWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _preferences.initialize(),
      builder: (context, _) => Drawer(
        child: LayoutBuilder(
          builder: (context, constraint) {
            return Card(
              color: Constants.cardColor,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  button(Icons.repeat, 'auto-play', 'auto play'),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  button(var icon, var preference, var text) {
    bool p = _preferences.get(preference);
    var c = p
        ? Constants.theme.colorScheme.secondary
        : Constants.theme.primaryColor;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Tooltip(
          message: text,
          child: TextButton(
            child: Icon(icon, color: c, size: 32),
            onPressed: () {
              setState(
                () {
                  _preferences.toggle(preference);
                },
              );
            },
          ),
        )
      ],
    );
  }

  final Preferences _preferences = Preferences();
}
