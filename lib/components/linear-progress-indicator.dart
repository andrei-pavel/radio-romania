import 'package:flutter/material.dart';

import 'package:podcasts/models/constants.dart';

class SmallHeightLinearProgressIndicator extends StatelessWidget {
  const SmallHeightLinearProgressIndicator(this._progress, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _progress < 1.0
        ? SizedBox(
            height: 1.0,
            child: LinearProgressIndicator(
                value: _progress,
                valueColor: AlwaysStoppedAnimation(
                    Constants.theme.colorScheme.secondary)))
        : Container();
  }

  final double _progress;
}
