import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:universal_platform/universal_platform.dart';

class Preferences {
  factory Preferences() {
    return _preferences;
  }

  Preferences._internal();

  initialize() async {
    if (isWeb()) {
      _uniqueVersion = '1.0';
    } else {
      final PackageInfo packageInfo = await PackageInfo.fromPlatform();
      _uniqueVersion =
          '${packageInfo.packageName}-${packageInfo.version}-${packageInfo.buildNumber}';
    }
    _sharedPreferences = await SharedPreferences.getInstance();
    if (getVersioned('podcasts-cleared') == null) {
      _sharedPreferences!.remove('podcasts').then(
        (bool cleared) {
          if (cleared) {
            setVersioned('podcasts-cleared', true);
          }
        },
      );
    }
  }

  dynamic get(String key) {
    if (key.isNotEmpty &&
        _sharedPreferences != null &&
        _sharedPreferences!.containsKey(key)) {
      return _sharedPreferences!.get(key);
    }
    return _defaults[key];
  }

  set(String key, dynamic value) async {
    if (key.isNotEmpty && value != null && _sharedPreferences != null) {
      if (value is bool) {
        return _sharedPreferences!.setBool(key, value);
      } else if (value is double) {
        return _sharedPreferences!.setDouble(key, value);
      } else if (value is int) {
        return _sharedPreferences!.setInt(key, value);
      } else if (value is String) {
        return _sharedPreferences!.setString(key, value);
      } else if (value is List<String>) {
        return _sharedPreferences!.setStringList(key, value);
      }
    }
  }

  getVersioned(String key) {
    return get(_uniqueVersion + key);
  }

  bool isDesktop() {
    return UniversalPlatform.isDesktop;
  }

  bool isWeb() {
    return UniversalPlatform.isWeb;
  }

  setVersioned(String key, dynamic value) async {
    return set(_uniqueVersion + key, value);
  }

  void toggle(String key) async {
    bool value = get(key);
    set(key, !value);
  }

  void clear() {
    for (String i in _defaults.keys) {
      if (!_notCleared.contains(i)) {
        _sharedPreferences?.remove(i);
      }
    }
  }

  final _defaults = {
    'auto-play': true,
    'favorites': '{"collection-length":0}',
    'last-month-retrieved': 'ianuarie',
    'last-retrieved': 0,
    'old-podcasts': '{"collection-length":0}',
    'podcasts': '{"collection-length":0}',
    'privacy-policy-acknowledged': false,
  };

  final _notCleared = ['favorites', 'privacy-policy-acknowledged'];
  static final Preferences _preferences = Preferences._internal();
  SharedPreferences? _sharedPreferences;
  late String _uniqueVersion;
}
