import 'dart:async';

import 'package:audio_service/audio_service.dart';
import 'package:audio_session/audio_session.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:just_audio/just_audio.dart';

import 'package:podcasts/models/constants.dart';
import 'package:podcasts/models/entry.dart';
import 'package:podcasts/models/preferences.dart';

class AudioState with ChangeNotifier {
  get currentlyPlaying => _playerAudioHandler.currentlyPlaying;

  get durationText => _format(_duration);

  get durationValueNonNegative => _durationValue <= 0.0 ? 1.0 : _durationValue;

  get isLive =>
      currentlyPlaying != null && currentlyPlaying!.title == Constants.liveText;

  get isPlaying => _playbackState?.playing;

  get isStopped => _playbackState?.processingState == AudioProcessingState.idle;

  get positionText => _format(_position);

  get positionToDouble =>
      _position != null ? _position.inMilliseconds.toDouble() : 0.0;

  get positionValue => positionToDouble > durationValueNonNegative
      ? durationValueNonNegative
      : positionToDouble;

  get urlExists => currentlyPlaying != null && currentlyPlaying!.urlExists;

  get _duration => _playerAudioHandler.duration;
  get _position => _playbackState?.position;
  get _durationValue => _duration.inMilliseconds.toDouble();

  Future start() async {
    _audioHandler = await AudioService.init(
      builder: () => SwitchAudioHandler(BaseAudioHandler()),
      config: AudioServiceConfig(
        androidNotificationChannelName: 'Media Controls',
        notificationColor: Constants.theme.colorScheme.primary,
        androidNotificationIcon: 'mipmap/ic_launcher',
        androidNotificationOngoing: true,
        androidStopForegroundOnPause: true,
        fastForwardInterval: const Duration(seconds: 15),
        rewindInterval: const Duration(seconds: 15),
      ),
    );

    _audioHandler.playbackState.listen(
      (PlaybackState state) {
        _playbackState = state;
        notifyListeners();
      },
    );

    _audioHandler.mediaItem.listen(
      (MediaItem? item) {
        notifyListeners();
      },
    );

    _audioHandler.customEvent.listen((event) async {
      if (event['name'] == 'currentlyPlaying') {
        Entry e = Entry.fromJSON(event['value']);
        await mediaItem(e).then(
          (var m) async {
            if (m != null) {
              await _playerAudioHandler.playMediaItem(m);
              await AudioService.androidForceEnableMediaButtons();
            }
          },
        );
      }
    });

    _audioHandler.inner = _playerAudioHandler;

    notifyListeners();
  }

  Future<Duration?> setEntry(Entry e) {
    return _playerAudioHandler.setEntry(e);
  }

  Future customAction(String name, Map<String, dynamic>? arguments) async {
    await _audioHandler.customAction(name, arguments);
    notifyListeners();
  }

  Future customActionCurrentlyPlaying() async {
    return customAction(
        'currentlyPlaying', {'value': currentlyPlaying.toJSON()});
  }

  void next() {
    _playerAudioHandler.skipToNext();
    notifyListeners();
  }

  Future pause() async {
    if (isPlaying) {
      await _audioHandler.pause();
      notifyListeners();
    }
  }

  void previous() {
    _playerAudioHandler.skipToPrevious();
    notifyListeners();
  }

  Future seek(final Duration i) async {
    await _audioHandler.seek(i);
    notifyListeners();
  }

  Future stop() async {
    await _audioHandler.stop();
    notifyListeners();
  }

  Future play() async {
    await customActionCurrentlyPlaying();
    await _audioHandler.play();
    notifyListeners();
  }

  Future windUp(final int i) async {
    if (isLive) {
      return;
    }
    var attemptedDuration = _position + Duration(seconds: i);
    if (attemptedDuration.inSeconds < 0) {
      attemptedDuration = const Duration(seconds: 0);
    } else if (attemptedDuration.inSeconds > _duration.inSeconds) {
      attemptedDuration = _duration;
    }
    await seek(attemptedDuration);
    notifyListeners();
  }

  String _format(final Duration input) {
    int seconds = input.inSeconds;
    if (seconds == 0) {
      return '0s';
    }
    int minutes = seconds ~/ 60;
    int hours = minutes ~/ 60;
    seconds %= 60;
    minutes %= 60;
    return (hours == 0 ? '' : '${hours}h ') +
        (minutes == 0 ? '' : '${minutes}m ') +
        (seconds == 0 ? '' : '${seconds}s');
  }

  Future mediaItem(Entry e) async {
    _playerAudioHandler.setEntry(e);
    Map<String, dynamic> extras = {};
    if (e.previous != null) {
      extras['hasPrevious'] = 'true';
      extras['previous'] = e.previous?.toJSON();
    } else {
      extras['hasPrevious'] = 'false';
    }
    if (e.next != null) {
      extras['hasNext'] = 'true';
      extras['next'] = e.next?.toJSON();
    } else {
      extras['hasNext'] = 'false';
    }
    return MediaItem(
      id: e.url,
      album: ' ',
      title: e.title,
      artist: e.podcast,
      duration: _duration,
      artUri: Constants.artURI,
      extras: extras,
    );
  }

  late SwitchAudioHandler _audioHandler;
  PlaybackState? _playbackState;
  final PlayerAudioHandler _playerAudioHandler = PlayerAudioHandler();
}

class PlayerAudioHandler extends BaseAudioHandler {
  PlayerAudioHandler() {
    _init();
  }

  Duration duration = Duration.zero;
  get isPlaying => _audioPlayer.playing;
  Preferences preferences = Preferences();

  Future<void> _init() async {
    final session = await AudioSession.instance;
    await session.configure(const AudioSessionConfiguration.speech());

    _interruptionSubscription = session.interruptionEventStream.listen(
      (AudioInterruptionEvent event) {
        if (event.begin) {
          if (isPlaying) {
            pause();
            _interrupted = true;
          }
        } else {
          switch (event.type) {
            case AudioInterruptionType.pause:
            case AudioInterruptionType.duck:
              if (!isPlaying() && _interrupted) {
                play();
              }
              break;
            case AudioInterruptionType.unknown:
              break;
          }
          _interrupted = false;
        }
      },
    );

    // Handle unplugged headphones.
    _noiseSubscription = session.becomingNoisyEventStream.listen(
      (_) {
        if (isPlaying) {
          pause();
        }
      },
    );

    _durationSubscription = _audioPlayer.durationStream.listen(
      (Duration? d) {
        duration = d ?? Duration.zero;
        _broadcastState();
      },
    );

    _positionSubscription = _audioPlayer.positionStream.listen(
      (Duration position) {
        if (position != Duration.zero) {
          _position = position;
          _positionMark = _audioPlayer.duration ?? Duration.zero;
          _broadcastState();
        }
      },
    );

    _processingSubscription = _audioPlayer.processingStateStream.listen(
      (ProcessingState state) {
        switch (state) {
          case ProcessingState.completed:
            _position = Duration.zero;
            if (preferences.get('auto-play')) {
              if (currentlyPlaying!.next != null) {
                setEntry(currentlyPlaying!.next!);
              }
            } else {
              setEntry(currentlyPlaying!);
              _audioPlayer.pause();
            }
            break;
          default:
            break;
        }
      },
    );

    _sequenceStateSubscription = _audioPlayer.sequenceStateStream.listen(
      (SequenceState? state) {},
    );
  }

  Future playPause() {
    if (isPlaying) {
      return pause();
    }
    return play();
  }

  Future windUp(final int i) async {
    Duration attemptedDuration = _position + Duration(seconds: i);
    if (attemptedDuration.inSeconds < 0) {
      attemptedDuration = const Duration(seconds: 0);
    } else if (_audioPlayer.duration! < attemptedDuration) {
      attemptedDuration = _audioPlayer.duration!;
    }
    await _audioPlayer.seek(attemptedDuration);
  }

  @override
  Future fastForward() {
    return windUp(15);
  }

  @override
  Future pause() {
    return _audioPlayer.pause();
  }

  @override
  Future play() {
    if (_positionMark == _audioPlayer.duration) {
      return _audioPlayer.seek(_position).then((_) => _audioPlayer.play());
    } else {
      return _audioPlayer.play();
    }
  }

  @override
  Future rewind() {
    return windUp(-15);
  }

  @override
  Future seekBackward(bool begin) {
    return windUp(-15);
  }

  @override
  Future seekForward(bool begin) {
    return windUp(15);
  }

  @override
  Future skipToNext() async {
    if (currentlyPlaying!.next != null) {
      await setEntry(currentlyPlaying!.next!);
    }
  }

  @override
  Future skipToPrevious() async {
    if (currentlyPlaying!.previous != null) {
      await setEntry(currentlyPlaying!.previous!);
    }
  }

  @override
  Future seek(Duration position) {
    return _audioPlayer.seek(position);
  }

  Future<Duration?> setEntry(Entry e) async {
    currentlyPlaying = e;
    return _audioPlayer.setUrl(e.url);
  }

  final Completer _completer = Completer();

  @override
  Future stop() async {
    await _audioPlayer.dispose();
    _durationSubscription.cancel();
    _interruptionSubscription.cancel();
    _noiseSubscription.cancel();
    _positionSubscription.cancel();
    _processingSubscription.cancel();
    _sequenceStateSubscription.cancel();
    await _broadcastState();
    _completer.complete();
    await super.stop();
  }

  AudioProcessingState _getProcessingState() {
    switch (_audioPlayer.processingState) {
      case ProcessingState.idle:
        return AudioProcessingState.idle;
      case ProcessingState.loading:
        return AudioProcessingState.loading;
      case ProcessingState.buffering:
        return AudioProcessingState.buffering;
      case ProcessingState.ready:
        return AudioProcessingState.ready;
      case ProcessingState.completed:
        return AudioProcessingState.completed;
      default:
        throw Exception("Invalid state: ${_audioPlayer.processingState}");
    }
  }

  Future _broadcastState() async {
    playbackState.add(
      playbackState.value.copyWith(
        bufferedPosition: _audioPlayer.bufferedPosition,
        controls: [
          MediaControl.rewind,
          isPlaying ? MediaControl.pause : MediaControl.play,
          MediaControl.stop,
          MediaControl.fastForward,
        ],
        systemActions: {
          MediaAction.pause,
          MediaAction.play,
          MediaAction.seekBackward,
          MediaAction.seekForward,
          MediaAction.seek,
          MediaAction.stop,
        },
        playing: _audioPlayer.playing,
        updatePosition: _audioPlayer.position,
        processingState: _getProcessingState(),
        speed: _audioPlayer.speed,
      ),
    );
  }

  final AudioPlayer _audioPlayer = AudioPlayer();
  Entry? currentlyPlaying;
  bool _interrupted = false;
  late StreamSubscription<Duration?> _durationSubscription;
  late StreamSubscription<AudioInterruptionEvent> _interruptionSubscription;
  late StreamSubscription<void> _noiseSubscription;
  late StreamSubscription<Duration> _positionSubscription;
  late StreamSubscription<ProcessingState> _processingSubscription;
  late StreamSubscription<SequenceState?> _sequenceStateSubscription;
  Duration _position = Duration.zero;
  Duration _positionMark = Duration.zero;
}
