import 'dart:collection';
import 'dart:convert';

import 'package:podcasts/models/constants.dart';

// One entry in the multilevel list displayed by this app
class Entry {
  String podcast = '';
  String title = '';
  String url = '';
  Entry? previous;
  Entry? next;
  bool isNew = false;
  bool isOld = false;
  bool playable = true;
  bool favorite = false;
  List<Entry> children = [];

  Entry(
    this.title, {
    this.url = '',
    this.podcast = '',
    this.isNew = false,
    this.isOld = false,
    this.playable = true,
    this.favorite = false,
  });

  get urlExists => url.isNotEmpty;

  Entry.fromJSON(String json) : this._fromMap(jsonDecode(json));

  toggleFavorite() {
    favorite = !favorite;
  }

  String toJSON() {
    return jsonEncode(_toMap());
  }

  static LinkedHashMap<String?, Entry> collectionFromJSON(String json) {
    return _collectionFromMap(jsonDecode(json));
  }

  static String collectionToJSON(LinkedHashMap<String?, Entry?> collection) {
    return jsonEncode(_collectionToMap(collection));
  }

  bool isOldOrNot() {
    final howManyMonthsBackToCheck = {1};
    final lowerCaseName = title.toLowerCase();
    final DateTime now = DateTime.now();
    if (lowerCaseName.contains(now.year.toString())) {
      for (var month in Constants.months.keys) {
        if (lowerCaseName.contains(month)) {
          if (month == Constants.monthsByIndex[now.month]) {
            return false;
          }
          for (int i in howManyMonthsBackToCheck) {
            if (now.month > i) {
              if (month == Constants.monthsByIndex[now.month - i]) {
                return false;
              }
            }
          }
        }
      }
    }

    for (int i in howManyMonthsBackToCheck) {
      if (now.month == i) {
        if (lowerCaseName.contains((now.year - 1).toString())) {
          if (lowerCaseName.contains(Constants.monthsByIndex[12 - i + 1]!)) {
            return false;
          }
        }
      }
    }

    for (Entry child in children) {
      if (!child.isOldOrNot()) {
        return false;
      }
    }

    return true;
  }

  Entry._fromMap(Map json) {
    favorite = json['favorite'];
    podcast = json['podcast'];
    title = json['title'];
    url = json['url'];
    isNew = false;
    playable = json['playable'];
    Entry? previous;
    for (int i = 0; i < json['children.length']; ++i) {
      Entry e = Entry._fromMap(json['child$i']);
      if (previous != null) {
        previous.next = e;
        e.previous = previous;
      }
      previous = e;
      children.add(e);
    }
    isOld = isOldOrNot();
  }

  Map<String, dynamic> _toMap() {
    Map<String, dynamic> result = {
      'favorite': favorite,
      'podcast': podcast,
      'title': title,
      'url': url,
      'playable': playable,
    };
    result['children.length'] = children.length;
    for (int i = 0; i < children.length; ++i) {
      result['child$i'] = children[i]._toMap();
    }
    return result;
  }

  static LinkedHashMap<String?, Entry> _collectionFromMap(
      Map<String, dynamic> json) {
    LinkedHashMap<String?, Entry> collection = LinkedHashMap();
    for (int i = 0; i < json['collection-length']; ++i) {
      Entry e = Entry._fromMap(json['collection-child$i']);
      collection[e.title] = e;
    }
    return collection;
  }

  static Map<String, dynamic> _collectionToMap(
      LinkedHashMap<String?, Entry?> collection) {
    Map<String, dynamic> result = {
      'collection-length': collection.values.length
    };
    int i = 0;
    for (Entry? e in collection.values) {
      result['collection-child$i'] = e!._toMap();
      ++i;
    }
    return result;
  }
}
