import 'package:flutter/material.dart';

class Constants {
  static const backendAuthority = 'radio-romania.p.avel.ro';
  static final artURI = Uri.https(backendAuthority, '/icon-stereoscopic.png');
  static final backendURL = Uri.https(backendAuthority, '');
  static final cardColor = Colors.grey.shade900;
  static const episodesPerPodcast = 24;
  static final fadedOutColor = Colors.grey.shade500;
  static const liveText = ' LIVE';
  static const liveAddresses = {
    '3Net "Florian Pittiș"': 'http://media.3netmedia.ro:8000/Live128',
    'Actualități': 'http://streams.euroweb.ro:8006',
    'Antena Satelor': 'http://stream2.srr.ro:8046',
    'Cultural': 'http://stream2.srr.ro:8016',
    'eTeatru.ro': 'http://stream2.srr.ro:8078',
    'Internațional 1': 'http://streams.euroweb.ro:8056',
    'Internațional 2': 'http://streams.eur  oweb.ro:8066',
    'Internațional 3': 'http://streams.euroweb.ro:8074',
    'Muzical': 'http://stream2.srr.ro:8026',
    'Regional Brașov': 'http://stream2.srr.ro:8210',
    'Regional București FM': 'http://streams.euroweb.ro:8036',
    // 'Regional Cluj (nu difuzează)': 'http://89.238.227.114:8508',
    // 'Regional Constanța (nu difuzează)': 'http://live.radioconstanta.ro:9006',
    // 'Regional Craiova (nu difuzează)': 'http://smtpedf.digi-net.ro:8000',
    // 'Regional Iași (nu difuzează)': 'http://am.live.radioiasi.ro:8000',
    // 'Regional Reșița (nu difuzează)': 'http://91.200.122.190:8012',
    'Regional Târgu Mureș': 'http://streaming.radiomures.ro:8302',
    // 'Regional Timișoara (nu difuzează)': 'http://www.radio-timisoara.ro/live',
    'Vacanța': 'http://streams.euroweb.ro:8330',
  };
  static const months = {
    'ianuarie': 1,
    'februarie': 2,
    'martie': 3,
    'aprilie': 4,
    'mai': 5,
    'iunie': 6,
    'iulie': 7,
    'august': 8,
    'septembrie': 9,
    'octombrie': 10,
    'noiembrie': 11,
    'decembrie': 12,
  };
  static const monthsByIndex = {
    1: 'ianuarie',
    2: 'februarie',
    3: 'martie',
    4: 'aprilie',
    5: 'mai',
    6: 'iunie',
    7: 'iulie',
    8: 'august',
    9: 'septembrie',
    10: 'octombrie',
    11: 'noiembrie',
    12: 'decembrie',
  };
  static final newColor = Colors.red.shade900;
  static const podcastCount = 2; // 27
  static const timeout = Duration(seconds: 8);
  static const buffer = 8;

  static final maximumEntryCount =
      (podcastCount - 1) * (episodesPerPodcast + 1) +
          liveAddresses.length +
          1 -
          buffer;
  static const maximumNetworkHitCount =
      (podcastCount - 1) * (episodesPerPodcast + 1) - buffer;
  static final theme = ThemeData(
    brightness: Brightness.dark,
    colorScheme: ColorScheme(
      background: Colors.black,
      brightness: Brightness.dark,
      error: Colors.white,
      onPrimary: Colors.black,
      onBackground: Colors.white,
      onError: Colors.black,
      onSecondary: Colors.white,
      onSurface: Colors.black,
      primary: Colors.white,
      secondary: Colors.red.shade900,
      surface: Colors.white,
    ),
    primaryColor: Colors.white,
    scaffoldBackgroundColor: Colors.black,
  );
}
