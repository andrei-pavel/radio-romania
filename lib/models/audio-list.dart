import 'dart:async';
import 'dart:collection';
import 'dart:core';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:html/dom.dart' as dom;
import 'package:http/http.dart' as http;
import 'package:html/parser.dart' as parser;
import 'package:pool/pool.dart';
import 'package:retry/retry.dart';
import 'package:synchronized/synchronized.dart';

import 'package:podcasts/models/constants.dart';
import 'package:podcasts/models/entry.dart';
import 'package:podcasts/models/preferences.dart';

class AudioList with ChangeNotifier {
  get length => _podcasts.length + _oldPodcasts.length;

  void setFavorite(Entry episode) {
    Entry? e = _favorites[episode.podcast];
    if (episode.favorite) {
      if (e == null) {
        e = Entry(episode.podcast);
        e.children.add(episode);
        _favorites[episode.podcast] = e;
      } else {
        if (!e.children.contains(episode)) {
          e.children.add(episode);
          _favorites[episode.podcast] = e;
        }
      }
    } else {
      if (e != null) {
        if (e.children.contains(episode)) {
          e.children.remove(episode);
          if (e.children.isEmpty) {
            _favorites.remove(episode.podcast);
          } else {
            _favorites[episode.podcast] = e;
          }
        }
      }
    }
    _preferences.set('favorites', Entry.collectionToJSON(_favorites));
  }

  Future getPodcasts({bool force = true}) async {
    if (!_preferences.get('privacy-policy-acknowledged')) {
      return;
    }
    bool skip = await _lock.synchronized(
      () async {
        if (force) {
          return false;
        }
        var now = DateTime.now();

        // If a month has passed, clear preferences.
        var lastMonthRetrieved = _preferences.get('last-month-retrieved');
        if (lastMonthRetrieved == null || lastMonthRetrieved != now.month) {
          _preferences.clear();
          _preferences.set('last-month-retrieved', now.month);
          return false;
        }

        // If more than an hour has passed since last pull, don't skip.
        var lastRetrieved = _preferences.get('last-retrieved');
        if (lastRetrieved == null ||
            const Duration(hours: 1) <
                now.difference(
                    DateTime.fromMillisecondsSinceEpoch(lastRetrieved))) {
          _networkHits = Constants.maximumNetworkHitCount;
          return true;
        }
        return false;
      },
    );
    if (skip) {
      return;
    }
    _networkHits = 0;
    retry(
        () => http.get(Constants.backendURL).timeout(Constants.timeout).then(
              (http.Response response) {
                parser.parse(response.body).getElementsByTagName('a').forEach(
                  (dom.Element a) {
                    _pool.withResource(() => _handleRootA(a));
                  },
                );
              },
            ), retryIf: (e) {
      return e is SocketException || e is TimeoutException;
    });
  }

  void _handleRootA(dom.Element a) {
    a.getElementsByTagName('use').forEach((dom.Element use) {
      _pool.withResource(
        () => _handleRootUse(use, a),
      );
    });
  }

  void _handleRootUse(dom.Element use, dom.Element a) {
    String attributes = use.attributes.toString();
    if (attributes.contains('xlink:href: #folder')) {
      String podcast = fixDiacritics(a.attributes['href'].toString())
          .replaceAll('/', '')
          .replaceAll('.', '');
      _getEpisodes(podcast);
    }
  }

  Future initialize() async {
    await _lock.synchronized(
      () async {
        var storedFavorites = _preferences.get('favorites');
        var storedPodcasts = _preferences.get('podcasts');
        var storedOldPodcasts = _preferences.get('old-podcasts');
        var podcasts = Entry.collectionFromJSON(storedPodcasts);
        if (_podcasts.isEmpty) {
          if (podcasts.isEmpty) {
            _podcasts[Constants.liveText] = Entry(Constants.liveText);
            for (String i in Constants.liveAddresses.keys) {
              _podcasts[Constants.liveText]!.children.add(
                    Entry(
                      i,
                      url: Constants.liveAddresses[i]!,
                      podcast: Constants.liveText,
                    ),
                  );
            }
          } else {
            var favorites = Entry.collectionFromJSON(storedFavorites);
            for (String? i in favorites.keys) {
              _favorites[i] = favorites[i];
            }
            for (String? i in podcasts.keys) {
              _podcasts[i] = podcasts[i];
            }
            var oldPodcasts = Entry.collectionFromJSON(storedOldPodcasts);
            for (String? i in oldPodcasts.keys) {
              _oldPodcasts[i] = oldPodcasts[i];
            }
          }
        }
        notifyListeners();
        getPodcasts(force: podcasts.isEmpty);
        notifyListeners();
      },
    );
  }

  double progress() {
    double p = _entryCount() / Constants.maximumEntryCount;
    return p;
  }

  double networkProgress() {
    double p = _networkHits / Constants.maximumNetworkHitCount;
    return p;
  }

  networkComplete() async {
    while (networkProgress() < 1.0) {
      await Future.delayed(const Duration(milliseconds: 1000));
    }
    return;
  }

  resetNetworkHits() {
    _networkHits = 0;
  }

  toList() {
    List<Entry?> l = [];
    if (_favorites.isNotEmpty) {
      l.add(Entry('Favorite', isOld: true, playable: false));
      l.addAll(_favorites.values.toList(growable: false));
    }
    if (_podcasts.isNotEmpty) {
      l.add(Entry('Emisiuni', isOld: true, playable: false));
      l.addAll(_podcasts.values.toList(growable: false));
    }
    if (_oldPodcasts.isNotEmpty) {
      l.add(Entry('Emisiuni străvechi', isOld: true, playable: false));
      l.addAll(_oldPodcasts.values.toList(growable: false));
    }
    return l;
  }

  int _comparator(Entry? a, Entry? b) {
    var wordsA = a!.title.split(' ');
    var wordsB = b!.title.split(' ');
    if (wordsA.length != wordsB.length) {
      return a.title.compareTo(b.title);
    }
    for (int i = wordsA.length - 1; i >= 0; --i) {
      var wordA = wordsA[i];
      var wordB = wordsB[i];
      var nA = int.tryParse(wordA);
      var nB = int.tryParse(wordB);
      if (nA != null && nB != null) {
        var diff = nA - nB;
        if (diff == 0) {
          continue;
        } else {
          return diff;
        }
      }
      var lowercaseWordA = wordA.toLowerCase();
      var lowercaseWordB = wordB.toLowerCase();
      if (Constants.months.containsKey(lowercaseWordA) &&
          Constants.months.containsKey(lowercaseWordB)) {
        var diff = Constants.months[lowercaseWordA]! -
            Constants.months[lowercaseWordB]!;
        if (diff == 0) {
          continue;
        } else {
          return diff;
        }
      }
    }
    return a.title.toLowerCase().compareTo(b.title.toLowerCase());
  }

  int _entryCount() {
    int count = _podcasts.length + _oldPodcasts.length;
    for (Map p in {_podcasts, _oldPodcasts}) {
      for (Entry? e in p.values as Iterable<Entry?>) {
        count += e!.children.length;
      }
    }
    return count;
  }

  _getEpisode(String podcast, String episode, Uri url) async {
    retry(
      () => http.get(url).timeout(Constants.timeout).then(
        (http.Response response) async {
          parser.parse(response.body).getElementsByTagName('a').forEach(
            (dom.Element a) {
              _pool.withResource(() => _handleEpisodeA(a, podcast, episode));
            },
          );
          ++_networkHits;
        },
      ),
      retryIf: (e) => e is SocketException || e is TimeoutException,
    );
  }

  _handleEpisodeA(dom.Element a, String podcast, String episode) {
    bool found = false;
    bool isNew = _isNew(episode);
    Entry dummy = Entry(episode);
    bool isOld = dummy.isOldOrNot();
    String href = a.attributes['href'].toString();
    for (var p in {_podcasts, _oldPodcasts}) {
      for (String? podcastKey in p.keys) {
        final Entry podcastEntry = p[podcastKey]!;
        if (podcastEntry.title == podcast) {
          found = true;
          bool episodeFound = false;
          for (Entry e in podcastEntry.children) {
            if (e.title == episode) {
              episodeFound = true;
              e.isNew = isNew;
              e.isOld = e.isOldOrNot();
              break;
            }
          }
          if (!episodeFound) {
            podcastEntry.children.add(Entry(
              episode,
              url: href,
              podcast: podcast,
              isNew: isNew,
              isOld: isOld,
            ));

            // Normalize new entries.
            if (!podcastEntry.isNew && isNew) {
              podcastEntry.isNew = true;
            }

            // Normalize old entries.
            if (podcastEntry.isOld && !isOld) {
              podcastEntry.isOld = false;
              _podcasts[podcastKey] = podcastEntry;
              p.remove(podcastKey);
            }
          }
          break;
        }
      }
    }
    if (!found) {
      Map p;
      if (isOld) {
        p = _oldPodcasts;
      } else {
        p = _podcasts;
      }
      var parent = Entry(
        podcast,
        isNew: isNew,
        isOld: isOld,
      );
      var child = Entry(
        episode,
        url: href,
        podcast: podcast,
        isNew: isNew,
        isOld: isOld,
      );
      parent.children.add(child);
      p[podcast] = parent;
    }

    if (_podcasts.length + _oldPodcasts.length >= Constants.podcastCount) {
      if (progress() >= 1.0) {
        _sort();

        // Populate nexts.
        Entry? previous;
        for (Entry? podcastEntry in _podcasts.values) {
          for (int j = 0; j < podcastEntry!.children.length; ++j) {
            if (previous != null) {
              previous.next = podcastEntry.children[j];
            }
            previous = podcastEntry.children[j];
          }
        }
        _preferences.set('podcasts', Entry.collectionToJSON(_podcasts));
        _preferences.set('old-podcasts', Entry.collectionToJSON(_oldPodcasts));
      }
    }
  }

  bool _isNew(String name) {
    final lowerCaseName = name.toLowerCase();
    final DateTime now = DateTime.now();
    if (lowerCaseName.contains(now.year.toString())) {
      for (var month in Constants.months.keys) {
        if (lowerCaseName.contains(month)) {
          if (month == Constants.monthsByIndex[now.month]) {
            if (lowerCaseName.contains("din ${now.day}") ||
                lowerCaseName.contains("din ${now.day - 1}") ||
                lowerCaseName.contains("din 0${now.day}") ||
                lowerCaseName.contains("din 0${now.day - 1}")) {
              return true;
            }
          }
        }
      }
    }

    return false;
  }

  String fixDiacritics(String input) {
    return input
        .replaceAll('%20', ' ') // whitespace
        .replaceAll('%2520', ' ') // whitespace
        .replaceAll('%C3%A2', 'â')
        .replaceAll('%25C3%25A2', 'â')
        .replaceAll('%C4%83', 'ă')
        .replaceAll('%25C4%2583', 'ă')
        .replaceAll('%C5%9F', 'ş')
        .replaceAll('%25C5%259F', 'ş')
        .replaceAll('%C5%A3', 'ţ')
        .replaceAll('%25C5%25A3', 'ţ')
        .replaceAll('%C8%9B', 'ț')
        .replaceAll('%25C8%259B', 'ț');
  }

  void _getEpisodes(String podcast) async {
    final Uri url = Uri.https(Constants.backendAuthority, '/' + podcast);
    retry(
      () => http.get(url).timeout(Constants.timeout).then(
        (http.Response response) {
          parser.parse(response.body).getElementsByTagName('a').forEach(
            (dom.Element a) {
              _pool.withResource(() => _handlePodcastA(a, podcast));
              ++_networkHits;
            },
          );
        },
      ),
      retryIf: (e) => e is SocketException || e is TimeoutException,
    );
  }

  _handlePodcastA(dom.Element a, String podcast) {
    String href = a.attributes['href'].toString().replaceAll('./', '');
    if (href.toLowerCase().startsWith('edi')) {
      String prettyName = fixDiacritics(href)
          .replaceAll('editia', 'ediția')
          .replaceAll('ediția-din-', 'Ediția din ')
          .replaceAll('Editia', 'Ediția');
      prettyName = _monthNumbersToMonths(prettyName);
      String prettyPodcast = podcast
          .replaceAll('-', ' ')
          .replaceAll(' ca ', ' că ')
          .replaceAll(' in ', ' în ')
          .replaceAll('buna', 'bună')
          .replaceAll('caciula', 'căciulă')
          .replaceAll('colectia', 'colecția')
          .replaceAll('cresti', 'crești')
          .replaceAll('credintei', 'credinței')
          .replaceAll('dispozitia', 'dispoziția')
          .replaceAll('dumneavoastra reteta', 'dumneavoastră: rețeta')
          .replaceAll('dumneavoastra', 'dumneavoastră')
          .replaceAll('globala', 'globală')
          .replaceAll('gramatica', 'gramatică')
          .replaceAll('guracod', 'gură. Cod')
          .replaceAll('la la infinit', 'la - la + infinit')
          .replaceAll('lectia', 'lecția')
          .replaceAll('limba romana', 'limba română')
          .replaceAll('luminoasa', 'luminoasă')
          .replaceAll('muzica omul', 'muzică. Omul')
          .replaceAll('muzica   omul', 'muzică. Omul')
          .replaceAll('national', 'național')
          .replaceAll('n are', 'n-are')
          .replaceAll('obiectiv', 'Obiectiv')
          .replaceAll('povesti', 'povești')
          .replaceAll('rra', 'RRA')
          .replaceAll('romania', 'România')
          .replaceAll('sanatate', 'sănătate')
          .replaceAll('sitcom undeva', 'sitcom UNDEva')
          .replaceAll('stapanul', 'stăpânul')
          .replaceAll('vorba', 'vorbă');
      prettyPodcast = _capitalize(prettyPodcast);

      _getEpisode(
          prettyPodcast,
          prettyName,
          Uri.parse("https://" +
              Constants.backendAuthority +
              '/' +
              podcast +
              '/' +
              href));
    }
  }

  int _reverseComparator(Entry a, Entry b) {
    return _comparator(b, a);
  }

  String _capitalize(String s) {
    String buffer = '';
    if (s.isNotEmpty) {
      buffer = s[0].toUpperCase() + s.substring(1);
    }
    return buffer;
  }

  String _monthNumbersToMonths(String s) {
    var split = s.split(' ');
    if (split.length >= 3) {
      var secondSplit = split[2].split('-');
      if (secondSplit.length >= 3) {
        var m = int.tryParse(secondSplit[1]) ?? -1;
        if (m == -1) {
          // Leave it alone.
          return s;
        }
        s = '${split[0]} ${split[1]} ${secondSplit[0]} ${Constants.monthsByIndex[m]!} ${secondSplit[2]}';
        for (int i = 3; i < secondSplit.length; ++i) {
          s += secondSplit[i];
        }
      }
    }
    return s;
  }

  Future _sort() async {
    for (var p in {_favorites, _podcasts, _oldPodcasts}) {
      List<Entry?> sortedValues = p.values.toList(growable: false)
        ..sort(_comparator);
      LinkedHashMap<String?, Entry?> sortedMap = LinkedHashMap();
      for (Entry? e in sortedValues) {
        sortedMap[e!.title] = e;
      }
      p.clear();
      p.addAll(sortedMap);
      for (Entry? episode in p.values) {
        if (episode!.title != Constants.liveText) {
          episode.children.sort(_reverseComparator);
          if (episode.children.length > Constants.episodesPerPodcast) {
            episode.children.removeRange(
                Constants.episodesPerPodcast, episode.children.length);
          }
        }
      }
    }
    notifyListeners();
  }

  int _networkHits = 0;
  final LinkedHashMap<String?, Entry?> _favorites = LinkedHashMap();
  final LinkedHashMap<String?, Entry?> _podcasts = LinkedHashMap();
  final LinkedHashMap<String?, Entry?> _oldPodcasts = LinkedHashMap();
  final Lock _lock = Lock();
  final Pool _pool = Pool(1, timeout: const Duration(seconds: 8));
  final Preferences _preferences = Preferences();
}
