import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'package:podcasts/components/audio-list-widget.dart';
import 'package:podcasts/components/media-controls-widget.dart';
import 'package:podcasts/models/audio-list.dart';
import 'package:podcasts/models/audio-state.dart';
import 'package:podcasts/models/constants.dart';
import 'package:podcasts/models/preferences.dart';

class MainWidget extends StatefulWidget {
  const MainWidget({Key? key}) : super(key: key);

  @override
  MainWidgetState createState() => MainWidgetState();
}

class MainWidgetState extends State<MainWidget> with WidgetsBindingObserver {
  @override
  Widget build(BuildContext context) {
    if (_initialized) {
      if (_preferences.isWeb()) {
        _preferences.set('privacy-policy-acknowledged', true);
      }
      return MultiProvider(
        providers: [
          ChangeNotifierProvider<AudioList>.value(value: _audioList),
          ChangeNotifierProvider<AudioState>.value(value: _audioState),
        ],
        child: (_preferences.get('privacy-policy-acknowledged') ||
                _preferences.isDesktop() ||
                _preferences.isWeb())
            ? SafeArea(
                child: Center(
                  child: ConstrainedBox(
                    constraints: const BoxConstraints(
                      maxWidth: 512,
                    ),
                    child: Column(
                      children: [
                        const AudioListWidget(),
                        OrientationBuilder(
                            builder: (context, orientation) =>
                                (orientation == Orientation.portrait)
                                    ? const MediaControlsWidget()
                                    : Container()),
                      ],
                    ),
                  ),
                ),
              )
            : Column(
                children: [
                  Expanded(
                    child: WebView(
                      initialUrl: '',
                      javascriptMode: JavascriptMode.disabled,
                      onWebViewCreated:
                          (WebViewController webViewController) async {
                        String fileHtmlContents = await rootBundle
                            .loadString('assets/privacy-policy.html');
                        webViewController.loadUrl(
                          Uri.dataFromString(
                            fileHtmlContents,
                            mimeType: 'text/html',
                            encoding: Encoding.getByName('utf-8'),
                          ).toString(),
                        );
                      },
                    ),
                  ),
                  Container(
                    decoration: const BoxDecoration(
                      border: Border(
                          top: BorderSide(
                            color: Colors.red,
                            width: 1,
                          ),
                          bottom: BorderSide(
                            color: Colors.black,
                            width: 32,
                          )),
                    ),
                    child: TextButton(
                      child: const Text(
                        "I acknowledge.",
                        style: TextStyle(
                          fontSize: 48,
                        ),
                      ),
                      onPressed: () {
                        _preferences.set('privacy-policy-acknowledged', true);
                        setState(() {});
                        _audioList.getPodcasts(force: true);
                      },
                    ),
                  )
                ],
              ),
      );
    } else {
      return SpinKitWanderingCubes(color: Constants.theme.primaryColor);
    }
  }

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(this);
    _preferences.initialize().then(
      (_) async {
        await _audioState.start();
        await _audioList.initialize();
        setState(
          () {
            _initialized = true;
          },
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  final AudioList _audioList = AudioList();
  final AudioState _audioState = AudioState();
  bool _initialized = false;
  final Preferences _preferences = Preferences();
}
