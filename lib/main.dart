import 'dart:io';

import 'package:flutter/material.dart';

import 'package:podcasts/screens/main-widget.dart';
import 'package:podcasts/models/constants.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

void main() {
  HttpOverrides.global = new MyHttpOverrides();

  runApp(
    MaterialApp(
      home: Scaffold(body: MainWidget()),
      theme: Constants.theme,
    ),
  );
}
